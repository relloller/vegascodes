// BPBig2-socket.js
var _ = require('lodash');
var http = require('http');
var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
// var socketio = require('socket.io');
var methodOverride = require('method-override');

// var server = require('http').Server(app);
// var io = require('socket.io')(server);
var app = require('express')();
var api = require('./api/index.js');
app.use(morgan('dev'));


app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(bodyParser.json());

app.use(methodOverride());

app.use(express.static('public'));

// app.get('/', function(req,res){
// 	res.status(200).send('ok');
// });

app.use('/api', api);
// app.get()
// app.listen()

app.listen(process.env.PORT || 8080, function() {

    console.log("vegas.codes server listening");
});
