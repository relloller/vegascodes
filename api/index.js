// index.js api
var _ = require('lodash');
var express = require('express');
var router = express.Router();
var deck52 = [1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21,
    22,
    23,
    24,
    25,
    26,
    27,
    28,
    29,
    30,
    31,
    32,
    33,
    34,
    35,
    36,
    37,
    38,
    39,
    40,
    41,
    42,
    43,
    44,
    45,
    46,
    47,
    48,
    49,
    50,
    51,
    52
];


router.get('/deal', function(req, res) {
    var dealHand = drawN(shuffle(deck52), 5);
    res.status(200).json(dealHand);
});

router.post('/draw', function(req, res) {
     console.log('req.body', req.body);
    console.log('req.body.holdCards',req.body.holdCards);
    if(!req.body.holdCards) {
        console.log('undefinedholdcatds');
        req.body.holdCards = [];
    }
    var cards = {};
    cards.hold = req.body.holdCards.map(function(e) {if (typeof e === 'string') return parseInt(e);
        return e;});
    
    cards.dealt = req.body.dealtCards.map(function(e) {if (typeof e === 'string') return parseInt(e);
        return e;});
    
    console.log('cards',cards);
    var drawHand = deepClone(cards.dealt);
    console.log('cardsdealt', cards.dealt);
        console.log('drawHand', drawHand);
    // if(cards.hold === 0) {var drawCards}
    var drawCards = drawN(restoreDeck(cards.dealt), 5 - cards.hold.length);
    var indxArr = [0,1,2,3,4];
    var indxCount = 0;
   

    for(var i=0; i<5; i++) {
        if(!contains(cards.hold, drawHand[i])) {
            drawHand[i] = drawCards[indxCount];
            indxCount++;
        }
    }
    // var drawHand = drawCards.concat(cards.hold);
    // drawHand = drawHand.map(function(e) {
    //     if (typeof e === 'string') return parseInt(e);
    //     return e;
    // });
    console.log('cardsdealt', cards.dealt);
     console.log('drawCards', drawCards);
    console.log('drawHand', drawHand);
    res.status(201).json(drawHand);
});

function contains(arr, val) {
  for(var j=0; j<arr.length;j++) {
    if(arr[j] === val) return true;
  }
  return false;
}

 function deepClone(arr) {
        var arrCopy = [];
        for(var i=0; i<arr.length; i++) arrCopy[i] = arr[i];
        return arrCopy;
    };
function restoreDeck(dealtCards) {
    var cloneArr = [];
    // var cloneArr = [];
    for (var i = 0; i < 52; i++) {
        cloneArr[i] = i + 1;
    }
        // console.log('cloneArr',cloneArr, cloneArr.length);

    var dealtCardsClone = deepClone(dealtCards);
    var dealtCardsDesc = dealtCardsClone.sort(function(a, b){return a-b;}).reverse();

    eA(dealtCardsDesc, function(e) {
        console.log('e',e);
        cloneArr.splice(e - 1, 1);
    });
    // console.log('cloneArrAfter', cloneArr, cloneArr.length);
    return cloneArr;
};

function eA(arr, cb) {
    for (var i = 0, len = arr.length; i < len; i++) {
        cb(arr[i]);
    }
}

function shuffle(array) {
    var cloneArr = [];
    for (var i = 0; i < array.length; i++) {
        cloneArr[i] = array[i];
    }
    var i = 0,
        j = 0,
        temp = null;

    for (i = cloneArr.length - 1; i > 0; i -= 1) {
        j = Math.floor(Math.random() * (i + 1));
        temp = cloneArr[i];
        cloneArr[i] = cloneArr[j];
        cloneArr[j] = temp;
    }
    return cloneArr;
};


function drawN(arr, numCards) {
    if(numCards === 0) return;
    var hand = [];
    // drawOne(arr);
    timesDoClosure(numCards, drawOne, arr);

    function drawOne(arr1) {
        var rnd = Math.floor(Math.random() * (arr1.length));
        var card = arr1.splice(rnd, 1);
        hand.push(card[0]);
        // console.log('arr1',arr1.length);
        return arr1;
    };
    return hand;
};

function timesDoClosure(num, fn, argInitOption) {
    if (argInitOption === 'undefined') {
        for (var i = 0; i < num; i++) {
            fn();
        }
    } else {
        var argNew = fn(argInitOption)
        for (var i = 1; i < num; i++) {
            argNew = fn(argNew);
            // console.log('timesDoClosure', argNew);
        }
        return argNew;
    }
};
module.exports = router;
